//
//  Common.h
//  CoolTable
//
//  Created by Stephen McCarthy on 04/02/2014.
//  Copyright (c) 2014 Stephen McCarthy. All rights reserved.
//

#import <Foundation/Foundation.h>

// We're not actually conctructing a class here, just defining a global function

void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor);

CGRect rectFor1PxStroke(CGRect rect); // modify the path to stroke so it takes the 1/2 pixel effect into consideration

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color);

void drawGlossAndGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor);

// methods to help us get the measurements of the arc under the footer

/*
 To make the arc we will use CGContextAddArc API, and as input to that function you have to tell graphics three things.
 1. The center point of the circle
 2. The radius of the circle
 3. The start point and end point of the line to draw
 
 // Ref: Intersecting Chord Theorem http://www.mathopenref.com/chordsintersecting.html
 
 */
static inline double radians (double degrees) { return degrees * M_PI/180; } // convert degrees to radians

CGMutablePathRef createArcPathFromBottomOfRect(CGRect rect, CGFloat arcHeight); // special path variable so we can re-use to draw mulitple times
