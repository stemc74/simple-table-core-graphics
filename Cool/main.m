//
//  main.m
//  Cool
//
//  Created by Stephen McCarthy on 04/02/2014.
//  Copyright (c) 2014 Stephen McCarthy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CTAppDelegate class]));
    }
}
