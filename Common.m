//
//  Common.m
//  CoolTable
//
//  Created by Stephen McCarthy on 04/02/2014.
//  Copyright (c) 2014 Stephen McCarthy. All rights reserved.
//

#import "Common.h"

void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor)
{
    //Note: Coregraphics is a state machine - once you set something it stays that way till you change it back
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB(); // need a color space to draw our gradient
    CGFloat locations[] = { 0.0, 1.0 }; // set up an array to track the location of each color with the range of the gradient. A value of 0 would mean the start of the gradient, 1 would mean the end of the gradient

    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor]; // create an array to old the colors passed into our function as parameters
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations); // we create our gradient by passing in the color space, color array and locations previously made
    // Note we have to convert the NSArray to a CFArrayRef by casting
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect)); // calculate the start & end point of the gradient
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect)); // CGRectGetMidX is a helper function from CGGeometry.h
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0); // helps draw the gradient into the provided rectangle. Note: this only fills the entire drawing region with the gradient, no way to set it to fill a sub area of the with the gradient. We use CGContextClip to do this
    CGContextRestoreGState(context); // With CGContextSaveCGState/CGContextRestoreCGState you can save the current setup of your context to a stack, and then pop it back later when you’re done and get back to where you were
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);

    
} // end drawLinearGradient


// here you modify the rectangle so the edge is halfway through the inside pixel of the original rectangle, so the stroke behavior works correctly

CGRect rectFor1PxStroke(CGRect rect)
{
    return CGRectMake(rect.origin.x + 0.5, rect.origin.y + 0.5, rect.size.width - 1, rect.size.height - 1);
} // end rectFor1PxStroke


void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color)
{
    CGContextSaveGState(context); // save/restore the context so you don’t leave any of the changes you made around.
    CGContextSetLineCap(context, kCGLineCapSquare); // set the line cap of the line, meaning the line ends EXACTLY at the ending point of the line
    CGContextSetStrokeColorWithColor(context, color); // set the color
    CGContextSetLineWidth(context, 1.0); // set the line width
    CGContextMoveToPoint(context, startPoint.x + 0.5, startPoint.y + 0.5); // draw the actual line
    CGContextAddLineToPoint(context, endPoint.x + 0.5, endPoint.y + 0.5);
    CGContextStrokePath(context); // call CGContextStrokePath to stroke the line
    CGContextRestoreGState(context);
} // end draw1PxStroke

void drawGlossAndGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor)
{
    drawLinearGradient(context, rect, startColor, endColor);
    
    UIColor * glossColor1 = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.35];
    UIColor * glossColor2 = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.1];
    
    CGRect topHalf = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height/2);
    
    drawLinearGradient(context, topHalf, glossColor1.CGColor, glossColor2.CGColor);
} // end drawGlossAndGradient



// Ref: Intersecting Chord Theorem http://www.mathopenref.com/chordsintersecting.html

CGMutablePathRef createArcPathFromBottomOfRect(CGRect rect, CGFloat arcHeight)
{
    CGRect arcRect = CGRectMake(rect.origin.x, rect.origin.y + rect.size.height - arcHeight, rect.size.width, arcHeight);
    
    CGFloat arcRadius = (arcRect.size.height/2) + (pow(arcRect.size.width, 2) / (8*arcRect.size.height));
    CGPoint arcCenter = CGPointMake(arcRect.origin.x + arcRect.size.width/2, arcRect.origin.y + arcRadius);
    
    CGFloat angle = acos(arcRect.size.width / (2*arcRadius));
    CGFloat startAngle = radians(180) + angle;
    CGFloat endAngle = radians(360) - angle;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddArc(path, NULL, arcCenter.x, arcCenter.y, arcRadius, startAngle, endAngle, 0);
    CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect));
    
    return path;
}


