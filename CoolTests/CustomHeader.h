//
//  CustomHeader.h
//  CoolTable
//
//  Created by Stephen McCarthy on 04/02/2014.
//  Copyright (c) 2014 Stephen McCarthy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomHeader : UIView

@property (nonatomic, strong) UILabel * titleLabel; // label to display the header
@property (nonatomic, strong) UIColor * lightColor; // store the color for light/dark gradient
@property (nonatomic, strong) UIColor * darkColor; // store the color for light/dark gradient

@end
